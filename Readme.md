Ref : https://gitee.com/expli/gd32-f4xx-free-rtos

Version:
- GD32F4xx_Firmware_Library_V3.1.0  [2023-08-21]
- FreeRTOS V10.4.3 LTS Patch 3      [Sep 17, 2022]