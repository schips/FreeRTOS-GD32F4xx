/*
 * @Author: Li expli@qq.com
 * @Date: 2022-06-28 17:17:17
 * @LastEditors: Li expli@qq.com
 * @LastEditTime: 2022-06-28 19:41:08
 * @FilePath: 
 * @Description: https://gitee.com/expli/gd32-f4xx-free-rtos
 */

#include "gd32f4xx.h"
#include "systick.h"
#include <stdio.h>
#include "FreeRTOS.h"
#include "task.h"

#define START_TASK_PRIO 1

#define START_STK_SIZE 128

TaskHandle_t StartTask_Handler;

void start_task(void *pvParameters);

#define TASKA_PRIO 2
#define TASKA_STK_SIZE 50
TaskHandle_t TaskA_Handler;
void task_a(void *p_arg);

#define TASKB_PRIO 3
#define TASKB_STK_SIZE 50
TaskHandle_t TaskB_Handler;
void task_b(void *p_arg);

int testA = 0;
int testB = 0;

/*!
    \brief      main function
    \param[in]  none
    \param[out] none
    \retval     none
*/
int main(void)
{
    /* configure systick */
    systick_config();

    xTaskCreate((TaskFunction_t)start_task,
                (const char *)"start_task",
                (uint16_t)START_STK_SIZE,
                (void *)NULL,
                (UBaseType_t)START_TASK_PRIO,
                (TaskHandle_t *)&StartTask_Handler);
    vTaskStartScheduler();
}

void start_task(void *pvParameters)
{
    taskENTER_CRITICAL();
    //
    xTaskCreate((TaskFunction_t)task_a,
                (const char *)"task_a",
                (uint16_t)TASKA_STK_SIZE,
                (void *)NULL,
                (UBaseType_t)TASKA_PRIO,
                (TaskHandle_t *)&TaskA_Handler);
    //    //
    xTaskCreate((TaskFunction_t)task_b,
                (const char *)"task_b",
                (uint16_t)TASKB_STK_SIZE,
                (void *)NULL,
                (UBaseType_t)TASKB_PRIO,
                (TaskHandle_t *)&TaskB_Handler);

    vTaskDelete(StartTask_Handler);
    taskEXIT_CRITICAL();
}

//中文
void task_a(void *pvParameters)
{
    while (1)
    {
        vTaskDelay(1000);
        testA++;
    }
}

void task_b(void *pvParameters)
{
    while (1)
    {
        testB++;
        vTaskDelay(500);
    }
}
